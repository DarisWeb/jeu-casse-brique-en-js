import {engine, render, runner} from "./game.js";
import {bonus} from "./bonus.js";
import {initball} from "./ballon.js";
import {addDuration, pauseTimer, removeDuration} from "./time.js";
import {countBrickDown, returnCountBrick} from "./brique.js";
import {play, minus, plus} from "./sounds.js";
import {addscore, endGameScore, getHighScore, getScore, storeToLocal} from "./score.js";

//detruit une brique qui rentre en collision avec le ballon ou le tir , ajoute des points au score et creer un bonus si la brique en detient un

export function deleteBrick() {
    Events.on(engine, 'collisionStart', function (event) {
        let pairs = event.pairs;

        for (let i = 0; i < pairs.length; i++) {
            let pair = pairs[i];
            if (pair.bodyA.events != null && pair.bodyB.label == "ballon" || pair.bodyA.events != null && pair.bodyB.label == "fire") {
                //console.log(pair.bodyA, pair.bodyB);
                bonus(pair.bodyA);
                World.remove(engine.world, pair.bodyA);
                play();
            }

            if (pair.bodyA.label == "brick" && pair.bodyB.label == "ballon") {
                //console.log(pair.bodyA, pair.bodyB);
                World.remove(engine.world, pair.bodyA);
                addscore();
                play();
                countBrickDown();
                //console.log("nbBrick", returnCountBrick());
                if (returnCountBrick() == 0) { //Si la derniere brique est cassee
                    setTimeout(onEndBrick, 500);
                }
            }

            if ((pair.bodyB.label == "fire" && pair.bodyA.label == "brick") || (pair.bodyA.label == "fire" && pair.bodyB.label == "brick")) {
                //console.log("collide");
                //console.log(pair.bodyA, pair.bodyB);
                World.remove(engine.world, pair.bodyA);
                World.remove(engine.world, pair.bodyB);
                countBrickDown();
                //console.log(returnCountBrick());
                addscore();

                if (returnCountBrick() == 0) {
                    setTimeout(onEndBrick, 500);
                }
            }

        }
    });

}

//Quand la derniere brique est casse on execute la fin du jeu
//on vient apres l'arret du jeu stocker le score du joueur
//en localstorage puis on affiche l'ecran de fin correspondant

function onEndBrick() {

    Runner.stop(runner, engine);
    Render.stop(render);
    World.clear(engine.world);
    Engine.clear(engine);
    pauseTimer();
    endGameScore();
    let finalscore = getScore();
    //console.log(finalscore);
    storeToLocal();
    getHighScore();
    //render.canvas.remove();
    render.canvas = null;
    render.context = null;
    render.textures = {};


    document.querySelector('.signin').style.display = 'none';
    document.querySelector('.affiche').style.display = 'none';
    document.querySelector('.dernier').style.display = 'block';
    document.querySelector('.over').style.display = 'block';

    document.querySelector('.gagne').style.display = 'block';
    document.querySelector('.rejouer').style.display = 'block';
    document.querySelector('.tableauScore').style.display = 'block';
    document.getElementById('pont').innerText = finalscore;
}

//detection quand un bonus rentre en collision avec la bar et execute le bonus correspondant
//suppresion des tirs qui rentre en collision avec le plafond et un ballon

export function detecbonusbar() {
    Events.on(engine, 'collisionStart', function (event) {
        let pairs = event.pairs;

        for (let i = 0; i < pairs.length; i++) {
            let pair = pairs[i];
            if (pair.bodyB.label == "bonusball" && pair.bodyA.label == "bar") {
                World.remove(engine.world, pair.bodyB);
                document.getElementById("bonus").innerText = "Tu as gagné 1 ballon!";
                initball();
                plus();
            } else if (pair.bodyB.label == "bonusscore" && pair.bodyA.label == "bar") {
                World.remove(engine.world, pair.bodyB);
                document.getElementById("bonus").innerText = "Tu as gagné 5 points!";
                addscore(5);
                plus();
            } else if (pair.bodyB.label == "malusscore" && pair.bodyA.label == "bar") {
                World.remove(engine.world, pair.bodyB);
                document.getElementById("bonus").innerText = "Tu as perdu 5 points!";
                addscore(-5);
                minus();
            } else if (pair.bodyB.label == "bonustime" && pair.bodyA.label == "bar") {
                World.remove(engine.world, pair.bodyB);
                document.getElementById("bonus").innerText = "Tu as gagné 3s!";
                addDuration(3);
                plus();
            } else if (pair.bodyB.label == "malustime" && pair.bodyA.label == "bar") {
                World.remove(engine.world, pair.bodyB);
                document.getElementById("bonus").innerText = "Tu as perdu 3s!";
                removeDuration(3);
                minus();
            }
            if (pair.bodyB.label == "topwall" && pair.bodyA.label == "fire") {
                //console.log("collide3");

                World.remove(engine.world, pair.bodyA);

            }
            if (pair.bodyA.label == "topwall" && pair.bodyB.label == "fire") {
                //console.log("collide4");

                World.remove(engine.world, pair.bodyB);

            }

            if (pair.bodyB.label == "ball" && pair.bodyA.label == "fire") {
                //console.log("collide3");

                World.remove(engine.world, pair.bodyA);

            }
            if (pair.bodyA.label == "ball" && pair.bodyB.label == "fire") {
                //console.log("collide4");

                World.remove(engine.world, pair.bodyB);

            }

        }

    });
}