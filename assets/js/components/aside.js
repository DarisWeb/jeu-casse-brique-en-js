import {
    game
} from "../main.js";
import {
    prenom,
    avatar
} from "./joueur.js";

//le lancemenet du jeu AllDarSo
let blockEnter = false;
export function gameEnter() {

    let button = document.getElementById("submit");
    button.addEventListener("click", onSubmit, false);

    document.addEventListener("keydown", event => {
        if (event.code === 'Enter') {
            console.log(blockEnter);
            if (blockEnter == false){
                onSubmit(event);
            }
        }

    });
    // il se lance avec le button:
    function onSubmit(e) {
        e.preventDefault();
        let inputName = document.getElementById("myname");

        // nom obligatoire:
        if (inputName.value.length == 0) {
            document.getElementById("nomobligatoire").innerText = "Ton nom est nécessaire!!";

        } else {
            blockEnter = true;
            document.querySelector('.affiche').style.display = 'flex';
            document.querySelector('.signin-info').style.display = 'none';
            game();
            prenom();
            avatar();
        }
    }
}
//après le fin du jeu, reload la page pour pouvoir rejouer sans relancer la page
export function rejouer() {
    let buttonrejouer = document.getElementById("rejouer");
    buttonrejouer.addEventListener("click", onSubmit, false);

    function onSubmit(p) {

        p.preventDefault();
        document.location.reload();
    }
}