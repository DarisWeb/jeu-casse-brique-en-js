import {
    engine,
    render
} from "./game.js";

let tabbar = initBar();
let c = tabbar[0]; //bodie composé
export let barGlobal = tabbar[1];
let ballBarLeftG = tabbar[2];
let ballBarRightG = tabbar[3];
var rightPressed = false;
var leftPressed = false;
let barY = 560;      //le y de la bar
let elmC = document.querySelector("#myCanvas");




export function drawbr() {
    let dx = 30; //deplacement de la ball sur touche fleches


    // eventListeners pour les touches et la souris:

    document.addEventListener("keydown", onDownPress, false);
    document.addEventListener("keyup", onUpPress, false);
    document.addEventListener("mousemove", mouseMove, false);

    Matter.Events.on(engine, 'beforeUpdate', function () {

        if (rightPressed) {

            Matter.Composite.translate(c, {
                x: 10,
                y: 0
            });
        } else if (leftPressed) {
            Matter.Composite.translate(c, {
                x: -10,
                y: 0
            });
        }

        if (barGlobal.position.x > 910) {
            Body.setPosition(barGlobal, {
                x: 910,
                y: barY
            });
            Body.setPosition(ballBarLeftG, {
                x: 835,
                y: barY
            });
            Body.setPosition(ballBarRightG, {
                x: 985,
                y: barY
            });
        } else if (barGlobal.position.x < 90) {
            Body.setPosition(barGlobal, {
                x: 90,
                y: barY
            });
            Body.setPosition(ballBarLeftG, {
                x: 15,
                y: barY
            });
            Body.setPosition(ballBarRightG, {
                x: 165,
                y: barY
            });
        }


    });

}

export function initBar() {     //Creation de la bar
    let startbarx = 500;        //les parametres de la bar
    let startbary = 560;
    let barwidth = 150;
    let barheight = 20;
    let ballbar_radius = 10;
    
    
    //la bar du millieu
    let bar = Bodies.rectangle(startbarx, startbary, barwidth, barheight, {     
        isStatic: true,
        label: 'bar',
        inertia: Infinity,
        restitution: 1,
        render: {
            sprite: {
                texture: 'assets/image/bar/ufo.png',
                xScale: 0.28,
                yScale: 0.13
            }
        }

    });
    // les balls (cercles) qui font partie de la bar (sur les cotes)
    let ballBarLeft = Bodies.circle(425, 560, ballbar_radius, {
        isStatic: true,
        render: {
            sprite: {
                texture: 'assets/image/bar/bouledefeu.png',
                xScale: 0.08,
                yScale: 0.08
            }
        }

    });

    let ballBarRight = Bodies.circle(575, 560, ballbar_radius, {
        isStatic: true,
        render: {
            sprite: {
                texture: 'assets/image/bar/bouledefeu.png',
                xScale: 0.08,
                yScale: 0.08
            }
        }

    });
    let c = Matter.Composite.create();       //ce composite inclus la bar du millieu et les 2 cercles sur les cotes
    Matter.Composite.add(c, bar);            //les 3 composants font la bar (raquette) finale
    Matter.Composite.add(c, ballBarLeft);
    Matter.Composite.add(c, ballBarRight);

    World.add(engine.world, [c]);
    return [c, bar, ballBarLeft, ballBarRight]; // retourne le composite et ses composants
}

//*******  Le deplacement de la bar avec la souris  *****/
export function mouseMove(e){
    let ballbar_radius = 10;
    let barwidth = 150;
    let barheight = 20;
    let winWidth = window.innerWidth;
    let CanvOffsetLeft = elmC.offsetLeft;
    let relativeX = e.clientX-CanvOffsetLeft;
    let barTotalWidth = barwidth + ballbar_radius;

    if(relativeX > barTotalWidth/2 && relativeX < 1000 - barTotalWidth/2) {
        
        Body.setPosition(barGlobal, {x:relativeX, y:barY} ); 
        Body.setPosition(ballBarLeftG, {x:relativeX - 75, y:barY} ); 
        Body.setPosition(ballBarRightG, {x:relativeX + 75, y:barY} ); 
    }
    else if(relativeX > 1000 - barTotalWidth/2){
        Body.setPosition(barGlobal, {x:910, y:barY} ); 
        Body.setPosition(ballBarLeftG, {x:835, y:barY} ); 
        Body.setPosition(ballBarRightG, {x:985, y:barY} ); 
    }
    else if(relativeX < barTotalWidth/2){
        Body.setPosition(barGlobal, {x:90, y:barY} ); 
        Body.setPosition(ballBarLeftG, {x:15, y:barY} ); 
        Body.setPosition(ballBarRightG, {x:165, y:barY} ); 
    }
}

/******  Le deplacement de la bar avec les touches  *******/
function onDownPress(e) {
    if (e.key == "Right" || e.key == "ArrowRight") {
        rightPressed = true;
       //console.log("droite");
    } else if (e.key == "Left" || e.key == "ArrowLeft") {
        leftPressed = true;
    }
}

function onUpPress(e) {
    if (e.key == "Right" || e.key == "ArrowRight") {
        rightPressed = false;
    } else if (e.key == "Left" || e.key == "ArrowLeft") {
        leftPressed = false;
    }
}